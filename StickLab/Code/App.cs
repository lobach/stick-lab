﻿using StickLab.Code.Preprocessors;
using StickLab.Code.Solvers;
using StickLab.Code.Variants;

namespace StickLab.Code;

public class App
{
    private enum AppMode
    {
        Single,
        Multiple
    }

    public int RepeatCount { get; set; }

    private readonly string _filePath;
    private readonly int _variantsCount;
    private readonly IPreprocessor _preprocessor;
    private readonly ISolver _solver;
    private readonly IterativeSolver _iterativeSolver;
    private readonly VariantsGenerator _variantsGenerator;
    private readonly AppMode _mode;

    private List<int> _parts;
    private int _maxLength;


    public App(string filePath, IPreprocessor preprocessor, ISolver solver)
    {
        _filePath = filePath;
        _preprocessor = preprocessor;
        _solver = solver;
        _mode = AppMode.Single;
    }

    public App(string filePath, int variantsCount, IPreprocessor preprocessor, IterativeSolver iterativeSolver,
        VariantsGenerator variantsGenerator)
    {
        _filePath = filePath;
        _variantsCount = variantsCount;
        _iterativeSolver = iterativeSolver;
        _variantsGenerator = variantsGenerator;
        _preprocessor = preprocessor;
        _iterativeSolver = iterativeSolver;
        _mode = AppMode.Multiple;
    }

    public List<SolveResult> Run(bool writeToConsole = false)
    {
        (_maxLength, _parts) = FileReader.ReadFile(_filePath);
        _parts = _preprocessor.Preprocess(_parts);
        if (_mode == AppMode.Single)
        {
            var result = _solver.Solve(_maxLength, new Variant(_parts));

            if (writeToConsole)
                Console.WriteLine(result.ToString());

            return new List<SolveResult> {result};
        }
        else
        {
            var result = _iterativeSolver.Solve(_maxLength, _variantsGenerator.Generate(_variantsCount, _parts, RepeatCount));
            if (writeToConsole)
                foreach (var res in result)
                    Console.WriteLine(res.ToString());

            return result;
        }
    }
}