﻿namespace StickLab.Code;

public static class FileReader
{
    public static (int, List<int>) ReadFile(string filePath)
    {
        var data = new List<int>();
        var lenStr = File
            .ReadAllLines(filePath)
            .First()
            .Split(' ');

        var maxLength = 0;
        for (var i = 0; i < lenStr.Length; i++)
        {
            var len = Convert.ToInt32(lenStr[i]);
            if (i == 0)
                maxLength = len;
            else
                data.Add(len);
        }

        return (maxLength, data);
    }
}