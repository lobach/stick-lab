﻿namespace StickLab.Code.Preprocessors;

public class HigherPreprocessor : IPreprocessor
{
    public List<int> Preprocess(List<int> parts)
    {
        parts.Sort((a, b) => a.CompareTo(b));
        return parts;
    }
}