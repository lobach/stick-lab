﻿namespace StickLab.Code.Preprocessors;

public interface IPreprocessor
{
    public List<int> Preprocess(List<int> parts);
}