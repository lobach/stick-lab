﻿using StickLab.Code.Variants;

namespace StickLab.Code.Solvers;

public interface ISolver
{
    public SolveResult Solve(int maxLength, Variant variant);
}