﻿using StickLab.Code.Variants;

namespace StickLab.Code.Solvers;

public class IterativeSolver
{
    private readonly ISolver _solver;

    public IterativeSolver(ISolver solver) => 
        _solver = solver;

    public List<SolveResult> Solve(int maxLength, List<Variant> variants) => 
        variants
            .Select(variant => _solver.Solve(maxLength, variant))
            .ToList();
}