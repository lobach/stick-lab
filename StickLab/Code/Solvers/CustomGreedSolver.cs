﻿namespace StickLab.Code.Solvers;

public class CustomGreedSolver : GreedSolver
{
    protected override void Execute(int from, int to, int maxLength, List<int> parts, List<Stick> sticks)
    {
        var max = (int) (to * 0.9f);
        for (int i = from, j = to - 1; i < max || j >= max; i++, j--)
        {
            if (i < max)
                LoopExecute(i, maxLength, parts, sticks);
            if (j >= max)
                LoopExecute(j, maxLength, parts, sticks);
        }
    }
}