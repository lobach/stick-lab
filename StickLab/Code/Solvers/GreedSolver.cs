﻿using StickLab.Code.Variants;

namespace StickLab.Code.Solvers;

public class GreedSolver : ISolver
{
    public virtual SolveResult Solve(int maxLength, Variant variant)
    {
        var parts = variant.Parts;
        var sticks = new List<Stick> {new(maxLength)};
        Execute(0, parts.Count, maxLength, parts, sticks);
        return new SolveResult(sticks);
    }

    protected static void AddStick(int maxLength, int part, List<Stick> sticks)
    {
        var newStick = new Stick(maxLength);
        if (!newStick.AllowAdd(part))
            throw new ArgumentOutOfRangeException();

        newStick.AddPart(part);
        sticks.Add(newStick);
    }

    protected virtual void Execute(int from, int to, int maxLength, List<int> parts, List<Stick> sticks)
    {
        for (var i = from; i < to; i++) 
            LoopExecute(i, maxLength, parts, sticks);
    }

    protected virtual void LoopExecute(int index, int maxLength, List<int> parts, List<Stick> sticks)
    {
        var part = parts[index];
        var partAdded = false;
        foreach (var stick in sticks)
        {
            if (stick.AllowAdd(part))
            {
                stick.AddPart(part);
                partAdded = true;
                break;
            }
        }

        if (!partAdded)
            AddStick(maxLength, part, sticks);
    }
}