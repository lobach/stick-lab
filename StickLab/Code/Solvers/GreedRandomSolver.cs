﻿using StickLab.Code.Variants;

namespace StickLab.Code.Solvers;

public class GreedRandomSolver : GreedSolver
{
    public override SolveResult Solve(int maxLength, Variant variant)
    {
        var parts = variant.Parts;
        var sticks = new List<Stick>();

        for (var i = 0; i < variant.SelectForce; i++)
            AddStick(maxLength, parts[i], sticks);

        //Execute(0, partsCount, maxLength, parts, sticks);
        Execute(variant.SelectForce, parts.Count, maxLength, parts, sticks);

        return new SolveResult(sticks);
    }

    protected override void Execute(int from, int to, int maxLength, List<int> parts, List<Stick> sticks)
    {
        var sticksIds = new List<int>();
        for (var i = from; i < to; i++)
            sticksIds.Add(i);
        sticksIds.Shuffle();

        foreach (var i in sticksIds)
            LoopExecute(i, maxLength, parts, sticks);
    }
}