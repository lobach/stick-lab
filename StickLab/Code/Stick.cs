﻿namespace StickLab.Code;

public class Stick
{
    public int MaxLength { get; }
    public List<int> Parts { get; }

    private int _currentLength;

    public Stick(int maxLength)
    {
        MaxLength = maxLength;
        Parts = new List<int>();
    }

    public Stick AddPart(int part)
    {
        Parts.Add(part);
        _currentLength += part;
        return this;
    }

    public bool AllowAdd(int part) =>
        _currentLength + part <= MaxLength;

    public override string ToString() => 
        Parts
            .Aggregate("", (current, part) => current + $"{part} ");
}