﻿namespace StickLab.Code;

public static class Extensions
{
    public static void Shuffle<T>(this IList<T> list) => 
        list.Shuffle(0);

    public static void Shuffle<T>(this IList<T> list, int from)
    {
        var random = new Random();
        var n = list.Count;
        while (n > from + 1)
        {
            n--;
            var k = random.Next(from, n + 1);
            (list[k], list[n]) = (list[n], list[k]);
        }
    }
    
    public static int Medium(this IList<int> list) => 
        list.Sum() / list.Count;
}