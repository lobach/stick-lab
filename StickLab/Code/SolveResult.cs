﻿namespace StickLab.Code;

public readonly struct SolveResult : IComparable<SolveResult>, IEquatable<SolveResult>
{
    public int SticksCount => Result.Count;
    public List<Stick> Result { get; }

    public SolveResult(List<Stick> result)
    {
        Result = result;
    }

    public override string ToString()
    {
        var str = "";
        str += SticksCount.ToString() + '\n';

        return Result
            .Aggregate(str, (current, stick) => current + (stick.ToString() + '\n'));
    }

    public int CompareTo(SolveResult other) => 
        SticksCount.CompareTo(other.SticksCount);

    public bool Equals(SolveResult other) => 
        SticksCount.Equals(other.SticksCount);

    public override bool Equals(object? obj) => 
        obj is SolveResult other && Equals(other);

    public override int GetHashCode() => 
        Result.GetHashCode();

    public static bool operator ==(SolveResult left, SolveResult right) => 
        left.Equals(right);

    public static bool operator !=(SolveResult left, SolveResult right) => 
        !(left == right);

    public static bool operator <(SolveResult self, SolveResult other) => 
        other > self;

    public static bool operator >(SolveResult self, SolveResult other) => 
        self.SticksCount > other.SticksCount;

    public static bool operator >=(SolveResult self, SolveResult other) => 
        self == other || self > other;

    public static bool operator <=(SolveResult self, SolveResult other) => 
        self == other || self < other;
}