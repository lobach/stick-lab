﻿namespace StickLab.Code.Variants;

public class CustomRandomVariantsGenerator : VariantsGenerator
{
    protected override Variant GenerateVariant(int runNumber, int totalCount, List<int> parts)
    {
        var med = parts.Medium();
        var begin = 0;
        for (var i = 0; i < parts.Count; i++)
            if (parts[i] < med)
            {
                begin = i;
                break;
            }

        var variant = new Variant(new List<int>(parts));
        variant.Parts.Shuffle(begin);
        return variant;
    }
}