﻿namespace StickLab.Code.Variants;

public class Variant
{
    public List<int> Parts { get; }
    public int SelectForce { get; set; }

    public Variant() => 
        Parts = new List<int>();

    public Variant(List<int> parts) => 
        Parts = parts;
}