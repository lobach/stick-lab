﻿namespace StickLab.Code.Variants;

public abstract class VariantsGenerator
{
    public virtual List<Variant> Generate(int count, List<int> parts, int repeat = 1)
    {
        var variants = new List<Variant>();

        for (var i = 0; i < count; i++)
        for (var j = 0; j < repeat; j++)
            variants.Add(GenerateVariant(i, count, parts));

        return variants;
    }

    protected abstract Variant GenerateVariant(int runNumber, int totalCount, List<int> parts);
}