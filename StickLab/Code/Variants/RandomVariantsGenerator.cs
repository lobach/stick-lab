﻿namespace StickLab.Code.Variants;

public class RandomVariantsGenerator : VariantsGenerator
{
    protected override Variant GenerateVariant(int runNumber, int totalCount, List<int> parts)
    {
        var partsCount = (int) (runNumber * 100.0f / (totalCount - 1));
        partsCount = Math.Clamp(partsCount, 0, parts.Count);
        var variant = new Variant(new List<int>(parts));
        variant.SelectForce = partsCount;
        variant.Parts.Shuffle(partsCount);
        return variant;
    }
}