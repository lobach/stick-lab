﻿using StickLab.Code;
using StickLab.Code.Preprocessors;
using StickLab.Code.Solvers;
using StickLab.Code.Variants;

namespace StickLab;

public static class Program
{
    private static bool Iterative = true;

    public static App App { get; set; }

    public static void Main(string[] args)
    {
        var runCount = 1;
        var preprocessor = new LowerPreprocessor();
        var dir = new DirectoryInfo("./Data");
        Console.WriteLine("=======================");
        foreach (var file in dir.GetFiles())
        {
            Console.WriteLine($"FILE: {file.Name}");
            Console.WriteLine("STOCK");
            RunGreed(file.FullName, preprocessor);
            RunIterative(runCount, file.FullName, preprocessor);

            Console.WriteLine("CUSTOM");
            RunCustomGreed(file.FullName, preprocessor);
            RunCustomIterative(runCount, file.FullName, preprocessor);

            Console.WriteLine("=======================");
        }
    }

    private static void RunIterative(int runCount, string filePath, IPreprocessor preprocessor)
    {
        Console.WriteLine("ITERATIVE");
        var variantsGenerator = new RandomVariantsGenerator();
        var solver = new IterativeSolver(new GreedSolver());
        RunIterativeInternal(runCount, filePath, preprocessor, solver, variantsGenerator);
    }

    private static void RunCustomIterative(int runCount, string filePath, IPreprocessor preprocessor)
    {
        Console.WriteLine("ITERATIVE CUSTOM");
        var variantsGenerator = new CustomRandomVariantsGenerator();
        var solver = new IterativeSolver(new CustomGreedSolver());
        RunIterativeInternal(runCount, filePath, preprocessor, solver, variantsGenerator);
    }

    private static void RunGreed(string filePath, IPreprocessor preprocessor)
    {
        Console.WriteLine("GREED");
        RunGreedInternal(filePath, preprocessor, new GreedSolver());
    }

    private static void RunCustomGreed(string filePath, IPreprocessor preprocessor)
    {
        Console.WriteLine("GREED CUSTOM");
        RunGreedInternal(filePath, preprocessor, new CustomGreedSolver());
    }

    private static void RunIterativeInternal(int runCount, string filePath, IPreprocessor preprocessor,
        IterativeSolver solver,
        VariantsGenerator variantsGenerator)
    {
        App = new App(filePath, runCount, preprocessor, solver, variantsGenerator);
        App.RepeatCount = runCount;
        var results = App.Run();
        results.Sort();
        Console.WriteLine("BEST:");
        Console.WriteLine(results.First().SticksCount);
    }

    private static void RunGreedInternal(string filePath, IPreprocessor preprocessor, ISolver solver)
    {
        App = new App(filePath, preprocessor, solver);
        var results = App.Run();
        results.Sort();
        Console.WriteLine("BEST:");
        Console.WriteLine(results.First().SticksCount);
    }
}